import cv2
from PIL import Image
print('Введите шаг чтения кадров')
step = int(input())         #для результативности лучше брать 500
vidcap = cv2.VideoCapture('traf.avi')
success,image = vidcap.read()
count = 0
if success:
    cv2.imwrite('frames/frame%d.png' % count, image)
    count += 1
while success:
    success,image = vidcap.read()
    if (count % step == 0):
        cv2.imwrite('frames/frame%d.png' % count, image)     # save frame as JPEG file      
        print('Read a new frame: ', success)
    count += 1
img = Image.open('frames/frame500.png').convert('LA')
img.save('greyscale.png')
img = cv2.imread('greyscale.png')
ret,thresh = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
cv2.imwrite('binary.png', thresh)
